﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Seconds_Minutes_Hours_Days
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void SecInput_TextChanged(object sender, EventArgs e)
        {

        }

        private void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                int seconds;
                int minutes;
                int hours;
                int days;

                secOutput.Text = "";
                daysOutput.Text = "";
                hoursOutput.Text = "";
                daysOutput.Text = "";

                seconds = int.Parse(secInput.Text);
                minutes = seconds / 60;
                hours = seconds / 3600;
                days = seconds / 86400;

                if (days > 0)
                {
                    daysOutput.Text = "There are " + days.ToString() + " days in " + seconds.ToString() + " seconds.";
                }
                if (hours > 0)
                {
                    hoursOutput.Text = "There are " + hours.ToString() + " hours in " + seconds.ToString() + " seconds.";
                }
                if (minutes > 0)
                {
                    secOutput.Text = "There are " + minutes.ToString() + " minutes in " + seconds.ToString() + " seconds.";
                }
                else
                {
                    secOutput.Text = "Not enough secs.";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void SecOutput_Click(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void HoursOutput_Click(object sender, EventArgs e)
        {

        }

        private void DaysOutput_Click(object sender, EventArgs e)
        {

        }
    }
}

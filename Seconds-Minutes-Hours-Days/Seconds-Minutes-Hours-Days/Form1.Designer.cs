﻿namespace Seconds_Minutes_Hours_Days
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.secInput = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.secOutput = new System.Windows.Forms.Label();
            this.hoursOutput = new System.Windows.Forms.Label();
            this.daysOutput = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // secInput
            // 
            this.secInput.Location = new System.Drawing.Point(111, 6);
            this.secInput.Name = "secInput";
            this.secInput.Size = new System.Drawing.Size(100, 20);
            this.secInput.TabIndex = 0;
            this.secInput.TextChanged += new System.EventHandler(this.SecInput_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(217, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Convert";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Seconds Elapsed:";
            // 
            // secOutput
            // 
            this.secOutput.AutoSize = true;
            this.secOutput.Location = new System.Drawing.Point(39, 53);
            this.secOutput.Name = "secOutput";
            this.secOutput.Size = new System.Drawing.Size(0, 13);
            this.secOutput.TabIndex = 3;
            this.secOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.secOutput.Click += new System.EventHandler(this.SecOutput_Click);
            // 
            // hoursOutput
            // 
            this.hoursOutput.AutoSize = true;
            this.hoursOutput.Location = new System.Drawing.Point(39, 86);
            this.hoursOutput.Name = "hoursOutput";
            this.hoursOutput.Size = new System.Drawing.Size(0, 13);
            this.hoursOutput.TabIndex = 4;
            this.hoursOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.hoursOutput.Click += new System.EventHandler(this.HoursOutput_Click);
            // 
            // daysOutput
            // 
            this.daysOutput.AutoSize = true;
            this.daysOutput.Location = new System.Drawing.Point(39, 122);
            this.daysOutput.Name = "daysOutput";
            this.daysOutput.Size = new System.Drawing.Size(0, 13);
            this.daysOutput.TabIndex = 5;
            this.daysOutput.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.daysOutput.Click += new System.EventHandler(this.DaysOutput_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 169);
            this.Controls.Add(this.daysOutput);
            this.Controls.Add(this.hoursOutput);
            this.Controls.Add(this.secOutput);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.secInput);
            this.Name = "Form1";
            this.Text = "Seconds-Minutes-Hours-Days";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox secInput;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label secOutput;
        private System.Windows.Forms.Label hoursOutput;
        private System.Windows.Forms.Label daysOutput;
    }
}

